<?php
function gilayon_post_type() {
  $labels = array(
    'name'               => _x( 'גליונות', 'post type general name' ),
    'singular_name'      => _x( 'גליון', 'post type singular name' ),
    'add_new'            => _x( 'הוסף חדש', 'gilayon' ),
    'add_new_item'       => __( 'הוסף גליון חדש' ),
    'edit_item'          => __( 'ערוך גליון' ),
    'new_item'           => __( 'גליון חדש' ),
    'all_items'          => __( 'כל הגליונות' ),
    'view_item'          => __( 'צפה בגליון' ),
    'search_items'       => __( 'חפש גליונות' ),
    'not_found'          => __( 'לא נמצאו גליונות' ),
    'not_found_in_trash' => __( 'לא נמצאו גליונות באשפה' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'גליונות'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Holds our Gilayons and product specific data',
    'public'        => true,
    'menu_position' => 5,
    'supports'      => array( 'title'),
    'has_archive'   => 'gilayon',
  );
  register_post_type( 'gilayon', $args ); 
}
add_action( 'init', 'gilayon_post_type' );
?>