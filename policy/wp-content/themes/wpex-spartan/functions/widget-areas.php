<?php
/**
 * Register our sidebars and widgetized areas.
 *
 */
function home_top_area() {

	register_sidebar( array(
		'name'          => 'Home Top',
		'id'            => 'home_top',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="rounded">',
		'after_title'   => '</h2>',
	) );

    register_sidebar( array(
        'name'          => 'Banner Between Articles',
        'id'            => 'home_post',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="rounded">',
        'after_title'   => '</h2>',
    ) );

}
add_action( 'widgets_init', 'home_top_area' );
?>