<?php

function get_gilayons(){
  $args = array( 
    "post_type" => "gilayon",
    "posts_per_page" => -1,
    "status" => "publish"
    );

  $gilayons = get_posts($args);
  return $gilayons;
}

function get_gilayons_sorted_by_date(){
  $gilayons = get_gilayons();
  $temp_years = array();
  
  foreach($gilayons as $gilayon){
    $temp_date = get_field("date",$gilayon->ID);
    $date = explode("/",$temp_date);

    $years[$date[0]] = array();
  }

  foreach($gilayons as $gilayon){ // goes through gilayons available for every registered year
    foreach($years as $year => $months){ // goes through registered years

    

      $temp_months = array(); // temporary months array to save the months registered

      $temp_date = get_field("date",$gilayon->ID); // Get the date of a Gilayon

      $date = explode("/",$temp_date); // the date exploded (yyyy , mm , dd) and saved as an array.
      //echo "Year: " . $year . "<br>";
      if($year == $date[0]){ // if the year registered equals the year of the gilayon
        //echo "month: " . $date[1] . "<br>";
        $temp_months[$date[1]] = array(); // add a key to the months array that equals to the month of the gilayon
        $years[$year] = $temp_months;
      }
      //echo "Years [" .$year. "] = ";
      
      //echo "<br>-----------------<br>";
    }

  }

  foreach($years as $year=>$months){ 

    foreach($months as $month => $ordered_gilayons){ 

      foreach($gilayons as $gilayon){ 

          $temp_gilayons = array();

          $temp_date = get_field("date",$gilayon->ID); // Get the date of a Gilayon
          $date = explode("/",$temp_date); // the date exploded (yyyy , mm , dd) and saved as an array.

          //echo "if( ". $year ." == " . $date[0] . "  && " .$month. " == ". $date[1] .")";
          //echo "<br>";

          if($year == $date[0] && $month == $date[1]){ 

            $temp_gilayons[] = $gilayon;
            $years[$year][$month][] = $gilayon;

          }
      }

    }

  }
 
  // foreach()
  //echo "<br>";
  //var_dump($years);
  return $years;
}







































/**
don't touch the code below
**/
function wp_get_cpt_archives( $cpt = 'gilayon', $args = array() ) {
  // if cpt is post run the core get archives
  if ( $cpt === 'post' ) return wp_get_archives($args); 
  // if cpt doesn't exists return error
  if ( ! post_type_exists($cpt) ) return new WP_Error('invalid-post-type');
  $pto = get_post_type_object($cpt);
  // if cpt doesn't have archive return error
  if ( ! $pto = get_post_type_object( $cpt ) ) return false;
  $types = array('monthly' => 'month', 'daily' => 'day', 'yearly' => 'year');
  $type = isset( $args['type'] ) ?  $args['type'] : 'monthly';
  if ( ! array_key_exists($type, $types) ) {
    // supporting only 'monthly', 'daily' and 'yearly' archives
     return FALSE;
  }

  $done_where = $done_link = FALSE;
  // filter where
  add_filter( 'getarchives_where' , function( $where ) use (&$done_where, $cpt) {
    if ( $done_where ) return $where;
    return str_replace( "post_type = 'post'" , "post_type = '{$cpt}'" , $where );
  });
  // filter link
  add_filter( "{$types[$type]}_link", function( $url ) use (&$done_link, $pto, $cpt) {
    if ( $done_link ) return $url;
     // if no pretty permalink add post_type url var
     // if ( get_option( 'permalink_structure' ) || ! $pto->rewrite ) {
     if ( ! get_option( 'permalink_structure' ) || ! $pto->rewrite ) {
       return add_query_arg( array( 'post_type' => $cpt ), $url );
     } else { // pretty permalink
       global $wp_rewrite;
       $slug = is_array( $pto->rewrite ) ? $pto->rewrite['slug'] : $cpt;
       $base = $pto->rewrite['with_front'] ? $wp_rewrite->front : $wp_rewrite->root;
       $home = untrailingslashit( home_url( $base ) );
       return str_replace( $home,  home_url( $base . $slug ), $url );
     }
  });
  // get original echo arg and then set echo to false  
  $notecho = isset($args['echo']) && empty($args['echo']);
  $args['echo'] = FALSE; 
  // get archives
  $archives = wp_get_archives($args);
  // prevent filter running again
  $done_where = $done_link = TRUE;
  // echo or return archives
  
  if ( $notecho ) { 
    return $archives;
  } else {
    echo $archives;
  }
}


function generate_cpt_archive_rules( $cpt ) { 
  if ( empty($cpt) || ! post_type_exists($cpt) ) return;
  global $wp_rewrite;
  $pto = get_post_type_object($cpt);
  if ( ! $pto->has_archive ) return;
  $base = $pto->rewrite['with_front'] ? $wp_rewrite->front : $wp_rewrite->root;
  $base = trailingslashit( $base );
  $slug = is_array( $pto->rewrite ) ? $pto->rewrite['slug'] : $cpt;
  $year = ltrim( $base . $slug . '/([0-9]{4})/?$', '/' );
  $month = ltrim( $base . $slug . '/([0-9]{4})/([0-9]{2})/?$', '/' );
  $day = ltrim( $base . $slug . '/([0-9]{4})/([0-9]{2})/([0-9]{2})/?$', '/' );
  $index = 'index.php?post_type=' . $cpt;
  $rules[$year] =  $index . '&m=$matches[1]';
  $rules[$month] = $index . '&m=$matches[1]$matches[2]';
  $rules[$day] = $index . '&m=$matches[1]$matches[2]$matches[3]';
  $page = 2;
  foreach ( array( $year, $month, $day ) as $rule ) {
    $paged = str_replace( '/?$', '/page/([0-9]{1,})/?$', $rule);
    $rules[$paged] = $rules[$rule] . '&paged=$matches[' . $page . ']';
    $page++;
  }
  return $rules;
}


function register_cpt_dates_rules() {
  $cpts = array( 'gilayon' );
  foreach ( $cpts as $cpt ) { 
    foreach ( $cpt_rules = generate_cpt_archive_rules( $cpt ) as $rule => $rewrite ) {
      add_rewrite_rule( $rule, $rewrite, 'top' );
    }
  }
}
// flushing on theme switch
add_action('after_switch_theme', function() {
  register_cpt_dates_rules();
  $GLOBALS['wp_rewrite']->flush_rules();
});
// registering on init
add_action( 'init', 'register_cpt_dates_rules' );

?>