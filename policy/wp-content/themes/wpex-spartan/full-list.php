<?php
/*
Template Name: רשימה מלאה
*/
get_header(); ?>

	<div id="primary" class="content-area clr">
		
		<div id="content" class="site-content left-content clr" role="main">
		<div class="title_gilayons"><h2>גיליונות קודמים</h2></div>
		<?php //wp_get_cpt_archives(); ?>
		<?php $years = get_gilayons_sorted_by_date() ?>
		<?php $countA=0;?>
		<?php foreach($years as $year => $months): ?>
			<?php $countA=$countA+1;
					$aaa="tempkcs".$countA;
			?>

			<div class="gilayon" id=<?php echo $aaa?> >
			<?php foreach($months as $month => $gilayons): ?>
				<h2 class="heading">
					<?php  $monthName = date("F", mktime(0, 0, 0, $month, 10));

						echo translate($monthName); 
						echo " ";
						echo $year;
						?>
				</h2>
				<div class="">
				<?php foreach($gilayons as $gilayon): ?>
					<a href = "<?php echo get_field('link', $gilayon->ID); ?>">
						<h3 class="gilayon-sub-title">
							<?php echo $gilayon->post_title; ?>
						</h3>
					</a>

					<p class="gilayon-p">
						<?php 

						$temp2=get_field("date",$gilayon->ID);
						
						$temp3=$temp2[5] . $temp2[6];
		
						//$temp2=str_replace("/","-",$temp2);
						
						//$time = strtotime($temp2);
						
						echo $temp3 ." ". translate($monthName) ." ". $year;

						?>
					</p>
					
				<?php endforeach; ?>
				</div>
			<?php endforeach; ?>
			</div>
		<?php endforeach; ?>
		</div><!-- #content -->
		<?php get_sidebar(); ?>
	</div><!-- #primary -->
<?php get_footer(); ?>

