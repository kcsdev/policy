<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package WordPress
 * @subpackage Spartan WPExplorer Theme
 * @since Spartan 1.0
 */

// Return on full-width post
if ( is_singular() && 'fullwidth' == get_post_meta( get_the_ID(), 'wpex_post_layout', true ) ) {
	return;
}

// Display sidebar if active
if ( is_active_sidebar( 'sidebar' ) ) : ?>
	<aside id="secondary" class="sidebar-container" role="complementary">
		<div class="sidebar-inner">
			<div class="widget-area"> <?php
                // Homepage slider alternative
$slider_alt = get_theme_mod( 'wpex_homepage_slider_alt' );

if ( $slider_alt ) {
	echo '<div id="home-slider-wrap" class="clr">'. do_shortcode( $slider_alt ) .'</div>';
	return;
}

// Get slider content
$slider_content = get_theme_mod( 'wpex_homepage_slider_content', 'recent_posts' );

// Show homepage featured slider if theme panel category isn't blank
if ( $slider_content && 'none' != $slider_content && get_theme_mod( 'wpex_homepage_slider_count', '3' ) >= 1 ) :

	if ( 'recent_posts' != $slider_content ) {
		$wpex_tax_query = array (
			array (
				'taxonomy'	=> 'category',
				'field'		=> 'ID',
				'terms'		=> $slider_content,
			),
		);
	} else {
		$wpex_tax_query = NULL;
	}
		
    $sidebar_id = get_field('sidebar_article')->ID;                         
    // Get posts based on featured category
	$wpex_query = new WP_Query( array(
		'post_type'				=>'post',
        'p'                     => $sidebar_id,
		'posts_per_page'		=> get_theme_mod( 'wpex_homepage_slider_count', '1' ),
		'no_found_rows'			=> true,
		'meta_key'				=> '_thumbnail_id',
		'tax_query'				=> $wpex_tax_query,
		'ignore_sticky_posts'	=> true,
	) );

	// Add filters for child thememing
	$wpex_query = apply_filters( 'wpex_featured_slider_query_args', $wpex_query );
	
	// If posts are found display slider
	if ( $wpex_query->have_posts() ) { ?>

				<?php 
				$wpex_count='0';
				// Loop through each post
				while( $wpex_query->have_posts() ) : $wpex_query->the_post();
				$wpex_count++;
					if ( has_post_thumbnail() ) {
						$img_width		= get_theme_mod( 'wpex_home_slider_img_width', '620' );
						$img_height		= get_theme_mod( 'wpex_home_slider_img_height', '350' );
						$img_crop		= ( '9999' == $img_height ) ? false : true;
						$cropped_image	= wpex_img_resize( wp_get_attachment_url( get_post_thumbnail_id() ), $img_width, $img_height, $img_crop );
						$excerpt		= wpex_excerpt( '25', false, false );
						if ( $cropped_image ) { ?>
						<div class="home-slider-slide" style='position:relative;height: auto;' data-dot="<?php echo $wpex_count; ?>">
							<div class="home-slider-media">
								<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( the_title_attribute( 'echo=0' ) ); ?>">
									<img src="<?php echo $cropped_image; ?>" alt="<?php echo the_title(); ?>" />
								</a>
							</div><!-- .home-slider-media -->
							<div class="home-slider-caption clr" style='position:static;margin-bottom: 20px; background:none;padding: 0;'>
								<h2 class="home-slider-caption-title">
									<a href="<?php the_permalink(); ?>" style='color:#474747;font-weight: bold;' title="<?php echo esc_attr( the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
								</h2>
								<?php if ( $excerpt ) { ?>
									<div class="home-slider-caption-excerpt clr" style='color:#474747;'>
										<?php echo get_field('description') ;?>
									</div><!-- .home-slider-caption-excerpt -->
								<?php } ?>
							</div><!--.home-slider-caption -->
						</div><!-- .home-slider-slide-->

				
					<?php 
                                                                     
					}
				}
				endwhile; ?>
	<?php } //End $wpex_query->have_posts check    
        
  //End $wpex_query->have_posts check
	// Reset post data to prevent query issues
	wp_reset_postdata();
endif; ?>
				<?php dynamic_sidebar( 'sidebar' ); ?>
			</div>
		</div>
	</aside><!-- #secondary -->
<?php endif; ?>