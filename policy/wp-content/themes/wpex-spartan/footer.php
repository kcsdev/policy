<?php
/**
 * The template for displaying the footer.
 *
 * @package WordPress
 * @subpackage Spartan WPExplorer Theme
 * @since Spartan 1.0
 */
?>

		</div><!--.site-main -->
	</div><!-- .site-main-wrap -->
</div><!-- #wrap -->

<?php get_template_part( 'partials/footer', 'layout' ); ?>
<?php get_template_part( 'partials/site', 'scrolltop' ); ?>

<?php wp_footer(); ?>

<script type="text/javascript">
	jQuery(document).ready(function($){
		$('#user_login').attr( 'placeholder', 'שם משתמש');
		$('#user_pass').attr( 'placeholder', 'סיסמא' );
	});
	
</script>
</body>
</html>