<?php
/**
 * General theme options
 *
 * @package		WordPress
 * @subpackage	Spartan WPExplorer Theme
 * @since		Spartan 1.0
 */

if( ! function_exists( 'wpex_customizer_general' ) ) {
	function wpex_customizer_general( $wp_customize ) {

		// Add General Panel
		$wp_customize->add_panel( 'wpex_general', array(
			'priority'			=> 140,
			'capability'		=> 'edit_theme_options',
			'title'				=> __( 'General', 'wpex' ),
		) );

		/*-----------------------------------------------------------------------------------*/
		/*	- Main
		/*-----------------------------------------------------------------------------------*/

		// Theme Settings Section
		$wp_customize->add_section( 'wpex_general' , array(
			'title'		=> __( 'Main', 'wpex' ),
			'priority'	=> 1,
			'panel'		=> 'wpex_general',
		) );

		// Responsive
		$wp_customize->add_setting( 'wpex_responsive', array(
			'type'				=> 'theme_mod',
			'default'			=> '1',
			'sanitize_callback' => false,
		) );
		$wp_customize->add_control( 'wpex_responsive', array(
			'label'		=> __( 'Enable Responsive Design', 'wpex' ),
			'section'	=> 'wpex_general',
			'settings'	=> 'wpex_responsive',
			'type'		=> 'checkbox',
			'priority'	=> '2',
		) );

		// Page Comments
		$wp_customize->add_setting( 'wpex_page_comments', array(
			'type'		=> 'theme_mod',
			'default'	=> '',
			'sanitize_callback' => false,
		) );
		$wp_customize->add_control( 'wpex_page_comments', array(
			'label'		=> __( 'Comments on Pages', 'wpex' ),
			'section'	=> 'wpex_general',
			'settings'	=> 'wpex_page_comments',
			'type'		=> 'checkbox',
			'priority'	=> '9',
		) );

		// Custom WP Gallery
		$wp_customize->add_setting( 'wpex_custom_wp_gallery_output', array(
			'type'		=> 'theme_mod',
			'default'	=> '1',
			'sanitize_callback' => false,
		) );
		$wp_customize->add_control( 'wpex_custom_wp_gallery_output', array(
			'label'		=> __( 'Custom WP Gallery Output', 'wpex' ),
			'section'	=> 'wpex_general',
			'settings'	=> 'wpex_custom_wp_gallery_output',
			'type'		=> 'checkbox',
			'priority'	=> '13',
		) );

		// Pagination
		$wp_customize->add_setting( 'wpex_pagination', array(
			'type'		=> 'theme_mod',
			'default'	=> 'pagination',
			'sanitize_callback' => false,
		) );
		$wp_customize->add_control( 'wpex_pagination', array(
			'label'		=> __( 'Pagination Style', 'wpex' ),
			'section'	=> 'wpex_general',
			'settings'	=> 'wpex_pagination',
			'priority'	=> '16',
			'type'		=> 'select',
			'choices'	=> array (
				'pagination'	=> __( 'Pagination', 'wpex' ),
				'next-prev'		=> __( 'Older/Newer', 'wpex' )
			)
		) );

		// Dashboard thumbns
		$wp_customize->add_setting( 'wpex_dashboard_thumbs', array(
			'type'		=> 'theme_mod',
			'default'	=> '1',
			'sanitize_callback' => false,
		) );
		$wp_customize->add_control( 'wpex_dashboard_thumbs', array(
			'label'		=> __( 'Post Thumbs In Admin Dashboard', 'wpex' ),
			'section'	=> 'wpex_general',
			'settings'	=> 'wpex_dashboard_thumbs',
			'type'		=> 'checkbox',
			'priority'	=> '18',
		) );

		// Featured category entry
		$wp_customize->add_setting( 'wpex_archive_feature_first_post', array(
			'type'		=> 'theme_mod',
			'default'	=> true,
			'sanitize_callback' => false,
		) );
		$wp_customize->add_control( 'wpex_archive_feature_first_post', array(
			'label'		=> __( 'Featured Category Entry', 'wpex' ),
			'section'	=> 'wpex_general',
			'settings'	=> 'wpex_archive_feature_first_post',
			'type'		=> 'checkbox',
			'priority'	=> '19',
		) );

		/*-----------------------------------------------------------------------------------*/
		/*	- Top Bar
		/*-----------------------------------------------------------------------------------*/

		// Theme Settings Section
		$wp_customize->add_section( 'wpex_topbar' , array(
			'title'		=> __( 'Top Bar', 'wpex' ),
			'priority'	=> 2,
			'panel'		=> 'wpex_general',
		) );

		// Top bar
		$wp_customize->add_setting( 'wpex_topbar', array(
			'type'		=> 'theme_mod',
			'default'	=> '1',
			'sanitize_callback' => false,
		) );
		$wp_customize->add_control( 'wpex_topbar', array(
			'label'		=> __( 'Enable Topbar', 'wpex' ),
			'section'	=> 'wpex_topbar',
			'settings'	=> 'wpex_topbar',
			'type'		=> 'checkbox',
			'priority'	=> '1',
		) );

		// Top bar date
		$wp_customize->add_setting( 'wpex_topbar_date', array(
			'type'		=> 'theme_mod',
			'default'	=> '1',
			'sanitize_callback' => false,
		) );
		$wp_customize->add_control( 'wpex_topbar_date', array(
			'label'		=> __( 'Enable Topbar Date', 'wpex' ),
			'section'	=> 'wpex_topbar',
			'settings'	=> 'wpex_topbar_date',
			'type'		=> 'checkbox',
			'priority'	=> '2',
		) );

		// Top bar search
		$wp_customize->add_setting( 'wpex_topbar_search', array(
			'type'		=> 'theme_mod',
			'default'	=> '1',
			'sanitize_callback' => false,
		) );
		$wp_customize->add_control( 'wpex_topbar_search', array(
			'label'		=> __( 'Enable Topbar Search', 'wpex' ),
			'section'	=> 'wpex_topbar',
			'settings'	=> 'wpex_topbar_search',
			'type'		=> 'checkbox',
			'priority'	=> '3',
		) );

		// Login/out page
		$choices = array(
			''			=> __( 'None', 'wpex' ),
		);

		$pages = get_pages( array(
			'number'	=> '100',
		) );
		if ( $pages ) {
			foreach ( $pages as $page ) {
				$choices[$page->ID] = $page->post_title;
			}
		}
		$wp_customize->add_setting( 'wpex_login_page', array(
			'type'		=> 'theme_mod',
			'default'	=> '',
			'sanitize_callback' => false,
		) );

		$wp_customize->add_control( 'wpex_login_page', array(
			'label'			=> __( 'Login Page URL', 'wpex' ),
			'section'		=> 'wpex_topbar',
			'settings'		=> 'wpex_login_page',
			'type'			=> 'select',
			'choices'		=> $choices,
			'priority'		=> '1',
			'description'	=> __( 'Select a page to add a login/logout link automatically to the end of the top bar menu.', 'wpex' ),
		) );


		/*-----------------------------------------------------------------------------------*/
		/*	- Header
		/*-----------------------------------------------------------------------------------*/

		// Theme Settings Section
		$wp_customize->add_section( 'wpex_header' , array(
			'title'		=> __( 'Header', 'wpex' ),
			'priority'	=> 3,
			'panel'		=> 'wpex_general'
		) );

		// Logo
		$wp_customize->add_setting( 'wpex_logo', array(
			'type'				=> 'theme_mod',
			'sanitize_callback' => false,
		) );
		$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'wpex_logo', array(
			'label'		=> __( 'Image Logo', 'wpex' ),
			'section'	=> 'wpex_header',
			'settings'	=> 'wpex_logo',
			'priority'	=> '1',
		) ) );

		// Retina Logo
		$wp_customize->add_setting( 'wpex_retina_logo', array(
			'type'		=> 'theme_mod',
			'default'	=> '',
			'sanitize_callback' => false,
		) );
		$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'wpex_retina_logo', array(
			'label'		=> __( 'Retina Image Logo', 'wpex' ),
			'section'	=> 'wpex_header',
			'settings'	=> 'wpex_retina_logo',
			'priority'	=> '2',
		) ) );

		// Retina Logo Height
		$wp_customize->add_setting( 'wpex_retina_logo_height', array(
			'type'		=> 'theme_mod',
			'default'	=> '',
			'sanitize_callback' => false,
		) );
		$wp_customize->add_control( 'wpex_retina_logo_height', array(
			'label'			=> __( 'Logo Image Height For Retina', 'wpex' ),
			'section'		=> 'wpex_header',
			'settings'		=> 'wpex_retina_logo_height',
			'priority'		=> '2',
			'type'			=> 'text',
			'description'	=> __( 'Enter the standard height in pixels of your standard logo. This is used to set your retina logo to the correct dimensions', 'wpex' ),
		) );

		// Subheading
		$wp_customize->add_setting( 'wpex_logo_subheading', array(
			'type'		=> 'theme_mod',
			'default'	=> __( 'Edit your subheading via the theme customizer.', 'wpex' ),
			'transport'	=> 'refresh',
			'sanitize_callback' => false,
		) );
		$wp_customize->add_control( 'wpex_logo_subheading', array(
			'label'		=> __( 'Subheading Under Logo', 'wpex' ),
			'section'	=> 'wpex_header',
			'settings'	=> 'wpex_logo_subheading',
			'type'		=> 'textarea',
			'priority'	=> '3',
		) );

		// Fixed Nav
		$wp_customize->add_setting( 'wpex_fixed_nav', array(
			'type'		=> 'theme_mod',
			'default'	=> '1',
			'sanitize_callback' => false,
		) );
		$wp_customize->add_control( 'wpex_fixed_nav', array(
			'label'		=> __( 'Fixed Navigation On Scroll', 'wpex' ),
			'section'	=> 'wpex_header',
			'settings'	=> 'wpex_fixed_nav',
			'type'		=> 'checkbox',
			'priority'	=> '4',
		) );

		// Mobile Menu Open Text
		$wp_customize->add_setting( 'wpex_mobile_menu_open_text', array(
			'type'				=> 'theme_mod',
			'default'			=> __( 'Browser Categories', 'wpex' ),
			'sanitize_callback' => false,
		) );
		$wp_customize->add_control( 'wpex_mobile_menu_open_text', array(
			'label'		=> __( 'Mobile Menu: Open Text', 'wpex' ),
			'section'	=> 'wpex_header',
			'settings'	=> 'wpex_mobile_menu_open_text',
			'type'		=> 'text',
			'priority'	=> '5',
		) );

		// Mobile Menu Open Text
		$wp_customize->add_setting( 'wpex_mobile_menu_close_text', array(
			'type'		=> 'theme_mod',
			'default'	=> __( 'Close navigation', 'wpex' ),
			'sanitize_callback' => false,
		) );
		$wp_customize->add_control( 'wpex_mobile_menu_close_text', array(
			'label'		=> __( 'Mobile Menu: Close Text', 'wpex' ),
			'section'	=> 'wpex_header',
			'settings'	=> 'wpex_mobile_menu_close_text',
			'type'		=> 'text',
			'priority'	=> '6',
		) );


		/*-----------------------------------------------------------------------------------*/
		/*	- Layouts
		/*-----------------------------------------------------------------------------------*/

		// Theme Settings Section
		$wp_customize->add_section( 'wpex_layouts' , array(
			'title'		=> __( 'Layouts', 'wpex' ),
			'priority'	=> 4,
			'panel'		=> 'wpex_general'
		) );

		// Site Layout
		$wp_customize->add_setting( 'wpex_entry_style', array(
			'type'		=> 'theme_mod',
			'default'	=> 'left-thumbnail',
			'sanitize_callback' => false,
		) );

		$wp_customize->add_control( 'wpex_entry_style', array(
			'label'		=> __( 'Default Entry Style','wpex'),
			'section'	=> 'wpex_layouts',
			'settings'	=> 'wpex_entry_style',
			'priority'	=> '1',
			'type'		=> 'select',
			'choices'	=> array (
				'left-thumbnail'	=> __( 'Left Thumbnail', 'wpex' ),
				'two-columns'		=> __( '2 Columns', 'wpex' ),
			)
		) );

		// Site Layout
		$wp_customize->add_setting( 'wpex_site_layout', array(
			'type'		=> 'theme_mod',
			'default'	=> 'right-sidebar',
			'sanitize_callback' => false,
		) );

		$wp_customize->add_control( 'wpex_site_layout', array(
			'label'		=> __( 'Site Layout','wpex'),
			'section'	=> 'wpex_layouts',
			'settings'	=> 'wpex_site_layout',
			'priority'	=> '2',
			'type'		=> 'select',
			'choices'	=> array (
				'right-sidebar'	=> __( 'Right Sidebar', 'wpex' ),
				'left-sidebar'	=> __( 'Left Sidebar', 'wpex' ),
			)
		) );

		// Footer Columns
		$wp_customize->add_setting( 'wpex_footer_columns', array(
			'type'		=> 'theme_mod',
			'default'	=> '4',
			'sanitize_callback' => false,
		) );
		$wp_customize->add_control( 'wpex_footer_columns', array(
			'label'		=> __( 'Footer Widget Columns','wpex'),
			'section'	=> 'wpex_layouts',
			'settings'	=> 'wpex_footer_columns',
			'priority'	=> '3',
			'type'		=> 'select',
			'choices'	=> array (
				'4'		=> '4',
				'3'		=> '3',
				'2'		=> '2',
				'1'		=> '1',
				'0'		=> '0',
			)
		) );


		/*-----------------------------------------------------------------------------------*/
		/*	- Category Colors
		/*-----------------------------------------------------------------------------------*/
		$cats = get_categories( array(
			'orderby'		=> 'name',
			'hide_empty'	=> false,
			//'parent'	=> 0
		) );

		// Theme Settings Section
		$wp_customize->add_section( 'wpex_category_colors' , array(
			'title'		=> __( 'Category Colors', 'wpex' ),
			'priority'	=> 5,
			'panel'		=> 'wpex_general',
		) );

		// Loop through categories and add color option
		if( $cats ) {
			foreach( $cats as $cat ) {
				$wp_customize->add_setting( 'wpex_cat_'. $cat->term_id .'_color', array(
					'type'		=> 'theme_mod',
					'default'	=> '',
					'sanitize_callback' => false,
				) );
				$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'wpex_cat_'. $cat->term_id .'_color', array(
					'label'		=> $cat->name,
					'section'	=> 'wpex_category_colors',
					'settings'	=> 'wpex_cat_'. $cat->term_id .'_color',
				) ) );
			}
		}


		/*-----------------------------------------------------------------------------------*/
		/*	- Ads
		/*-----------------------------------------------------------------------------------*/

		// Default ads
		$ad_620x80 = '<a href="#"><img src="'. get_template_directory_uri() .'/images/ad-620x80.png" /></a>';
		$ad_250 = '<a href="#"><img src="'. get_template_directory_uri() .'/images/ad-250x250.png" /></a>';

		// Theme Settings Section
		$wp_customize->add_section( 'wpex_ads' , array(
			'title'		=> __( 'Advertisements', 'wpex' ),
			'priority'	=> 6,
			'panel'		=> 'wpex_general',
		) );

		// Header Ad
		$wp_customize->add_setting( 'wpex-ad-header', array(
			'type'		=> 'theme_mod',
			'default'	=> $ad_620x80,
			'transport'	=> 'refresh',
			'sanitize_callback' => false,
		) );
		$wp_customize->add_control( 'wpex-ad-header', array(
			'label'			=> __( 'Header', 'wpex' ),
			'section'		=> 'wpex_ads',
			'settings'		=> 'wpex-ad-header',
			'type'			=> 'textarea',
			'priority'		=> '1',
		) );

		// Post Before media
		$wp_customize->add_setting( 'wpex-ad-post-before', array(
			'type'		=> 'theme_mod',
			'default'	=> $ad_620x80,
			'transport'	=> 'refresh',
			'sanitize_callback' => false,
		) );
		$wp_customize->add_control( 'wpex-ad-post-before', array(
			'label'			=> __( 'Post: Before Media/Title', 'wpex' ),
			'section'		=> 'wpex_ads',
			'settings'		=> 'wpex-ad-post-before',
			'type'			=> 'textarea',
			'priority'		=> '2',
		) );

		// Post Top Ad
		$wp_customize->add_setting( 'wpex-ad-post-top', array(
			'type'		=> 'theme_mod',
			'default'	=> $ad_250,
			'transport'	=> 'refresh',
			'sanitize_callback' => false,
		) );
		$wp_customize->add_control( 'wpex-ad-post-top', array(
			'label'			=> __( 'Post Content: Top', 'wpex' ),
			'section'		=> 'wpex_ads',
			'settings'		=> 'wpex-ad-post-top',
			'type'			=> 'textarea',
			'priority'		=> '3',
			'description'	=> __( 'This ad is floated to the left and a 250x250 or 300x250 ad will work best.', 'wpex' ),
		) );

		// Post Bottom Ad
		$wp_customize->add_setting( 'wpex-ad-post-bottom', array(
			'type'		=> 'theme_mod',
			'default'	=> $ad_620x80,
			'transport'	=> 'refresh',
			'sanitize_callback' => false,
		) );
		$wp_customize->add_control( 'wpex-ad-post-bottom', array(
			'label'			=> __( 'Post Content: Bottom', 'wpex' ),
			'section'		=> 'wpex_ads',
			'settings'		=> 'wpex-ad-post-bottom',
			'type'			=> 'textarea',
			'priority'		=> '4',
		) );

		// Home Top Ad
		$wp_customize->add_setting( 'wpex-ad-home-top', array(
			'type'		=> 'theme_mod',
			'default'	=> $ad_620x80,
			'transport'	=> 'refresh',
			'sanitize_callback' => false,
		) );
		$wp_customize->add_control( 'wpex-ad-home-top', array(
			'label'			=> __( 'Homepage: Top', 'wpex' ),
			'section'		=> 'wpex_ads',
			'settings'		=> 'wpex-ad-home-top',
			'type'			=> 'textarea',
			'priority'		=> '5',
		) );

		// Home Bottom Ad
		$wp_customize->add_setting( 'wpex-ad-home-bottom', array(
			'type'		=> 'theme_mod',
			'default'	=> $ad_620x80,
			'transport'	=> 'refresh',
			'sanitize_callback' => false,
		) );
		$wp_customize->add_control( 'wpex-ad-home-bottom', array(
			'label'			=> __( 'Homepage: Bottom', 'wpex' ),
			'section'		=> 'wpex_ads',
			'settings'		=> 'wpex-ad-home-bottom',
			'type'			=> 'textarea',
			'priority'		=> '6',
		) );

		// Archive Top Ad
		$wp_customize->add_setting( 'wpex-ad-archive-top', array(
			'type'		=> 'theme_mod',
			'default'	=> $ad_620x80,
			'transport'	=> 'refresh',
			'sanitize_callback' => false,
		) );
		$wp_customize->add_control( 'wpex-ad-archive-top', array(
			'label'			=> __( 'Archive: Top', 'wpex' ),
			'section'		=> 'wpex_ads',
			'settings'		=> 'wpex-ad-archive-top',
			'type'			=> 'textarea',
			'priority'		=> '7',
		) );

		// Archive Bottom Ad
		$wp_customize->add_setting( 'wpex-ad-archive-bottom', array(
			'type'		=> 'theme_mod',
			'default'	=> $ad_620x80,
			'transport'	=> 'refresh',
			'sanitize_callback' => false,
		) );
		$wp_customize->add_control( 'wpex-ad-archive-bottom', array(
			'label'			=> __( 'Archive: Bottom', 'wpex' ),
			'section'		=> 'wpex_ads',
			'settings'		=> 'wpex-ad-archive-bottom',
			'type'			=> 'textarea',
			'priority'		=> '8',
		) );


		/*-----------------------------------------------------------------------------------*/
		/*	- SEO
		/*-----------------------------------------------------------------------------------*/

		// Vars
		$html_tags = array(
			'span'	=> 'span',
			'div'	=> 'div',
			'h2'	=> 'h2',
			'h3'	=> 'h3',
			'h4'	=> 'h4',
			'h5'	=> 'h5',
		);

		// Theme Settings Section
		$wp_customize->add_section( 'wpex_seo' , array(
			'title'		=> __( 'SEO', 'wpex' ),
			'priority'	=> 6,
			'panel'		=> 'wpex_general',
		) );

		// Comments Link
		$wp_customize->add_setting( 'wpex_comment_author_link', array(
			'type'		=> 'theme_mod',
			'default'	=> '',
			'sanitize_callback' => false,
		) );
		$wp_customize->add_control( 'wpex_comment_author_link', array(
			'label'		=> __('Disable Links In Comments','wpex'),
			'section'	=> 'wpex_seo',
			'settings'	=> 'wpex_comment_author_link',
			'type'		=> 'checkbox',
			'priority'	=> '1',
		) );

		// Sidebar Widget Title Type
		$wp_customize->add_setting( 'wpex_sidebar_heading_tags', array(
			'type'		=> 'theme_mod',
			'default'	=> 'span',
			'sanitize_callback' => false,
		) );

		$wp_customize->add_control( 'wpex_sidebar_heading_tags', array(
			'label'		=> __( 'Sidebar Heading Tags','wpex'),
			'section'	=> 'wpex_seo',
			'settings'	=> 'wpex_sidebar_heading_tags',
			'priority'	=> '2',
			'type'		=> 'select',
			'choices'	=> $html_tags,
		) );

		// Footer Widget Title Type
		$wp_customize->add_setting( 'wpex_footer_heading_tags', array(
			'type'		=> 'theme_mod',
			'default'	=> 'span',
			'sanitize_callback' => false,
		) );

		$wp_customize->add_control( 'wpex_footer_heading_tags', array(
			'label'		=> __( 'Footer Heading Tags','wpex'),
			'section'	=> 'wpex_seo',
			'settings'	=> 'wpex_footer_heading_tags',
			'priority'	=> '3',
			'type'		=> 'select',
			'choices'	=> $html_tags,
		) );

		
		/*-----------------------------------------------------------------------------------*/
		/*	- Social Sharing
		/*-----------------------------------------------------------------------------------*/

		// Theme Settings Section
		$wp_customize->add_section( 'wpex_social_sharing' , array(
			'title'		=> __( 'Social Sharing', 'wpex' ),
			'priority'	=> 7,
			'panel'		=> 'wpex_general',
		) );

		// Enable/Disable Social at the top of posts
		$wp_customize->add_setting( 'wpex_social_sharing_post_top', array(
			'type'		=> 'theme_mod',
			'default'	=> '',
			'sanitize_callback' => false,
		) );
		$wp_customize->add_control( 'wpex_social_sharing_post_top', array(
			'label'		=> __( 'Enable Social Sharing Above Posts', 'wpex' ),
			'section'	=> 'wpex_social_sharing',
			'settings'	=> 'wpex_social_sharing_post_top',
			'type'		=> 'checkbox',
			'priority'	=> '2',
		) );

		// Enable/Disable Social at the top of posts
		$wp_customize->add_setting( 'wpex_social_sharing_post_bottom', array(
			'type'		=> 'theme_mod',
			'default'	=> '1',
			'sanitize_callback' => false,
		) );
		$wp_customize->add_control( 'wpex_social_sharing_post_bottom', array(
			'label'		=> __( 'Enable Social Sharing Below Posts', 'wpex' ),
			'section'	=> 'wpex_social_sharing',
			'settings'	=> 'wpex_social_sharing_post_bottom',
			'type'		=> 'checkbox',
			'priority'	=> '3',
		) );

		// Select Services
		$wp_customize->add_setting( 'wpex_social_share_services', array(
			'type'		=> 'theme_mod',
			'default'	=> array( 'twitter', 'facebook', 'google_plus', 'pinterest', 'linkedin' ),
			'sanitize_callback' => false,
		) );
		$wp_customize->add_control( new WPEX_Customize_Multicheck_Control( $wp_customize, 'wpex_social_share_services',
			array(
				'settings'	=> 'wpex_social_share_services',
				'label'		=> __( 'Enabled Services', 'wpex' ),
				'section'	=> 'wpex_social_sharing',
				'type'		=> 'multicheck',
				'priority'	=> '4',
				'choices'	=> array(
					'twitter'		=> 'Twitter',
					'facebook'		=> 'Facebook',
					'google_plus'	=> 'Google Plus',
					'pinterest'		=> 'Pinterest',
					'linkedin'		=> 'LinkedIn',
				)
			)
		) );


		/*-----------------------------------------------------------------------------------*/
		/*	- Copyright
		/*-----------------------------------------------------------------------------------*/

		// Copyright Section
		$wp_customize->add_section( 'wpex_copyright' , array(
			'title'		=> __( 'Copyright', 'wpex' ),
			'priority'	=> 8,
			'panel'		=> 'wpex_general',
		) );
		
		$wp_customize->add_setting( 'wpex_copyright', array(
			'type'		=> 'theme_mod',
			'default'	=> '',
			'sanitize_callback' => false,
		) );

		$wp_customize->add_control( 'wpex_copyright', array(
			'label'		=> __('Custom Copyright','wpex'),
			'section'	=> 'wpex_copyright',
			'settings'	=> 'wpex_copyright',
			'type'		=> 'textarea',
		) );

		/*-----------------------------------------------------------------------------------*/
		/*	- Entries
		/*-----------------------------------------------------------------------------------*/

		// Define Entries Section
		$wp_customize->add_section( 'wpex_entries' , array(
			'title'		=> __( 'Entries', 'wpex' ),
			'priority'	=> 9,
			'panel'		=> 'wpex_general',
		) );

		// Excerpt Length
		$wp_customize->add_setting( 'wpex_excerpt_length', array(
			'type'		=> 'theme_mod',
			'default'	=> '25',
			'sanitize_callback' => false,
		) );
		$wp_customize->add_control( 'wpex_excerpt_length', array(
			'label'		=> __( 'Excerpt Lengths', 'wpex' ),
			'section'	=> 'wpex_entries',
			'settings'	=> 'wpex_excerpt_length',
			'priority'	=> 1,
		) );

		// Readmore Text
		$wp_customize->add_setting( 'wpex_readmore_text', array(
			'type'		=> 'theme_mod',
			'default'	=> __( 'Read More', 'wpex' ),
			'sanitize_callback' => false,
		) );
		$wp_customize->add_control( 'wpex_readmore_text', array(
			'label'		=> __( 'Entry Read More Link Text', 'wpex' ),
			'section'	=> 'wpex_entries',
			'settings'	=> 'wpex_readmore_text',
			'priority'	=> 2,
		) );

		// Ignore More Tag
		$wp_customize->add_setting( 'wpex_ignore_more_tag', array(
			'type'		=> 'theme_mod',
			'default'	=> '',
			'sanitize_callback' => false,
		) );
		$wp_customize->add_control( 'wpex_ignore_more_tag', array(
			'label'		=> __( 'Ignore More Tag', 'wpex' ),
			'section'	=> 'wpex_entries',
			'settings'	=> 'wpex_ignore_more_tag',
			'type'		=> 'checkbox',
			'priority'	=> 3,
		) );

		// Entries display?
		$wp_customize->add_setting( 'wpex_entry_content_excerpt', array(
			'type'		=> 'theme_mod',
			'default'	=> 'excerpt',
			'sanitize_callback' => false,
		) );
		$wp_customize->add_control( 'wpex_entry_content_excerpt', array(
			'label'		=> __( 'Entries display?', 'wpex' ),
			'section'	=> 'wpex_entries',
			'settings'	=> 'wpex_entry_content_excerpt',
			'priority'	=> 4,
			'type'		=> 'select',
			'choices'	=> array (
				'excerpt'	=> __( 'The Excerpt', 'wpex' ),
				'content'	=> __( 'The Content', 'wpex' )
			)
		) );

		// Readmore
		$wp_customize->add_setting( 'wpex_blog_readmore', array(
			'type'		=> 'theme_mod',
			'default'	=> '',
			'sanitize_callback' => false,
		) );
		$wp_customize->add_control( 'wpex_blog_readmore', array(
			'label'		=> __( 'Entry Read More Link', 'wpex' ),
			'section'	=> 'wpex_entries',
			'settings'	=> 'wpex_blog_readmore',
			'type'		=> 'checkbox',
			'priority'	=> 5,
		) );

		// Entry Embeds
		$wp_customize->add_setting( 'wpex_entry_embeds', array(
			'type'		=> 'theme_mod',
			'default'	=> '',
			'sanitize_callback' => false,
		) );
		$wp_customize->add_control( 'wpex_entry_embeds', array(
			'label'		=> __( 'Entry Video/Audio Embeds', 'wpex' ),
			'section'	=> 'wpex_entries',
			'settings'	=> 'wpex_entry_embeds',
			'type'		=> 'checkbox',
			'priority'	=> 6,
		) );


		// Entry Lightbox
		$wp_customize->add_setting( 'wpex_entry_img_lightbox', array(
			'type'		=> 'theme_mod',
			'default'	=> '',
			'sanitize_callback' => false,
		) );
		$wp_customize->add_control( 'wpex_entry_img_lightbox', array(
			'label'		=> __( 'Entry Featured Image Lightbox', 'wpex' ),
			'section'	=> 'wpex_entries',
			'settings'	=> 'wpex_entry_img_lightbox',
			'type'		=> 'checkbox',
			'priority'	=> 7,
		) );

		// Entry Meta
		$wp_customize->add_setting( 'wpex_entry_meta', array(
			'type'		=> 'theme_mod',
			'default'	=> array( 'date', 'comments' ),
			'sanitize_callback' => false,
		) );
		$wp_customize->add_control( new WPEX_Customize_Multicheck_Control( $wp_customize, 'wpex_entry_meta',
			array(
				'settings'	=> 'wpex_entry_meta',
				'label'		=> __( 'Entry Meta Displays?', 'wpex' ),
				'section'	=> 'wpex_entries',
				'priority'	=> 8,
				'type'		=> 'multicheck',
				'choices'	=> array(
					'date'		=> __( 'Date', 'wpex' ),
					'author'	=> __( 'Author', 'wpex' ),
					'category'	=> __( 'Category', 'wpex' ),
					'comments'	=> __( 'Comments', 'wpex' ),
				)
			)
		) );

		/*-----------------------------------------------------------------------------------*/
		/*	- Posts
		/*-----------------------------------------------------------------------------------*/

		// Theme Settings Section
		$wp_customize->add_section( 'wpex_posts' , array(
			'title'		=> __( 'Posts', 'wpex' ),
			'priority'	=> 10,
			'panel'		=> 'wpex_general',
		) );

		// Post Thumb
		$wp_customize->add_setting( 'wpex_blog_post_thumb', array(
			'type'		=> 'theme_mod',
			'default'	=> '1',
			'sanitize_callback' => false,
		) );
		$wp_customize->add_control( 'wpex_blog_post_thumb', array(
			'label'		=> __( 'Featured Image on Posts', 'wpex' ),
			'section'	=> 'wpex_posts',
			'settings'	=> 'wpex_blog_post_thumb',
			'type'		=> 'checkbox',
			'priority'	=> 1,
		) );

		// Post Tags
		$wp_customize->add_setting( 'wpex_post_tags', array(
			'type'		=> 'theme_mod',
			'default'	=> '1',
			'sanitize_callback' => false,
		) );
		$wp_customize->add_control( 'wpex_post_tags', array(
			'label'		=> __( 'Post Tags', 'wpex' ),
			'section'	=> 'wpex_posts',
			'settings'	=> 'wpex_post_tags',
			'type'		=> 'checkbox',
			'priority'	=> 2,
		) );

		// Post Author
		$wp_customize->add_setting( 'wpex_post_author', array(
			'type'		=> 'theme_mod',
			'default'	=> '1',
			'sanitize_callback' => false,
		) );
		$wp_customize->add_control( 'wpex_post_author', array(
			'label'		=> __( 'Post Author Box', 'wpex' ),
			'section'	=> 'wpex_posts',
			'settings'	=> 'wpex_post_author',
			'type'		=> 'checkbox',
			'priority'	=> 3,
		) );

		// Related Posts
		$wp_customize->add_setting( 'wpex_related', array(
			'type'		=> 'theme_mod',
			'default'	=> '1',
			'sanitize_callback' => false,
		) );
		$wp_customize->add_control( 'wpex_related', array(
			'label'		=> __( 'Related Posts', 'wpex' ),
			'section'	=> 'wpex_posts',
			'settings'	=> 'wpex_related',
			'type'		=> 'checkbox',
			'priority'	=> 4,
		) );

		// Next Prev
		$wp_customize->add_setting( 'wpex_next_prev', array(
			'type'		=> 'theme_mod',
			'default'	=> '1',
			'sanitize_callback' => false,
		) );
		$wp_customize->add_control( 'wpex_next_prev', array(
			'label'		=> __( 'Next & Previous Post Links', 'wpex' ),
			'section'	=> 'wpex_posts',
			'settings'	=> 'wpex_next_prev',
			'type'		=> 'checkbox',
			'priority'	=> 5,
		) );

		// Post Meta
		$wp_customize->add_setting( 'wpex_post_meta', array(
			'type'		=> 'theme_mod',
			'default'	=> array( 'date', 'author', 'category', 'comments' ),
			'sanitize_callback' => false,
		) );
		$wp_customize->add_control( new WPEX_Customize_Multicheck_Control( $wp_customize, 'wpex_post_meta',
			array(
				'settings'	=> 'wpex_post_meta',
				'label'		=> __( 'Post Meta Displays?', 'wpex' ),
				'section'	=> 'wpex_posts',
				'priority'	=> 6,
				'type'		=> 'multicheck',
				'choices'	=> array(
					'date'		=> __( 'Date', 'wpex' ),
					'author'	=> __( 'Author', 'wpex' ),
					'category'	=> __( 'Category', 'wpex' ),
					'comments'	=> __( 'Comments', 'wpex' ),
				)
			)
		) );

	}
}
add_action( 'customize_register', 'wpex_customizer_general' );