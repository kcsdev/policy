<?php
/**
 * Homepage slider customizer settings
 *
 * @package WordPress
 * @subpackage Spartan WPExplorer Theme
 * @since Spartan 1.0
 */

if( ! function_exists( 'wpex_customizer_homepage' ) ) {
	function wpex_customizer_homepage( $wp_customize ) {

		// Add Homepage Panel
		$wp_customize->add_panel( 'wpex_homepage', array(
			'priority'       => 141,
			'capability'     => 'edit_theme_options',
			'title'          => __( 'Homepage', 'wpex' ),
		) );

		$slider_carousel_choices = array(
			'none'			=> __( 'None - Disable', 'wpex' ),
			'recent_posts'	=> __( 'Recent Posts', 'wpex' ),
		);

		// Main Cats Options
		$cats = get_categories();
		if ( $cats ) {
			foreach ( $cats as $cat ) {
				$slider_carousel_choices[$cat->term_id] = $cat->name;
			}
		}

		/*-----------------------------------------------------------------------------------*/
		/*	- Homepage Categories
		/*-----------------------------------------------------------------------------------*/
		
		// Get parent categories
		$parent_cats = get_categories( array(
					'orderby'	=> 'name',
 					'parent'	=> 0
		) );
		if ( $parent_cats ) {
			foreach ( $parent_cats as $cat ) {
				$slider_carousel_choices[$cat->term_id] = $cat->name;
			}
		}

		// Define Theme Settings Section
		$wp_customize->add_section( 'wpex_home_categories' , array(
			'title'			=> __( 'Homepage Categories', 'wpex' ),
			'priority'		=> 1,
			'panel'			=> 'wpex_homepage',
			'description'	=> __( 'By default the Hompeage template displays all parent categories and a list of the recent posts for that category. Here you can elect the categories to include. Uncheck any you want to exclude.', 'wpex' ),
		) );

		// Loop through categories and add color option
		if( $parent_cats ) {
			foreach( $parent_cats as $cat ) {
				$wp_customize->add_setting( 'wpex_home_cat_'. $cat->term_id, array(
					'type'		=> 'theme_mod',
					'default'	=> '1',
					'sanitize_callback' => false,
				) );
				$wp_customize->add_control( 'wpex_home_cat_'. $cat->term_id, array(
					'label'		=> $cat->name,
					'section'	=> 'wpex_home_categories',
					'settings'	=> 'wpex_home_cat_'. $cat->term_id,
					'type'		=> 'checkbox',
				) );
			}
		}

		// How many items to show per category
		$wp_customize->add_setting( 'wpex_homepage_cat_entry_count', array(
			'type'		=> 'theme_mod',
			'default'	=> '6',
			'sanitize_callback' => false,
		) );
		$wp_customize->add_control( 'wpex_homepage_cat_entry_count', array(
			'label'			=> __( 'Posts Per Category', 'wpex' ),
			'section'		=> 'wpex_home_categories',
			'settings'		=> 'wpex_homepage_cat_entry_count',
			'priority'		=> '999',
			'description'	=> __( 'How many posts do you wish to show per category. This number includes the first featured post', 'wpex' ),
		) );


		/*-----------------------------------------------------------------------------------*/
		/*	- Homepage Slider
		/*-----------------------------------------------------------------------------------*/

		// Theme Settings Section
		$wp_customize->add_section( 'wpex_slider' , array(
			'title'		=> __( 'Homepage Slider', 'wpex' ),
			'priority'	=> 2,
			'panel'		=> 'wpex_homepage',
		) );

		// Slider Content
		$wp_customize->add_setting( 'wpex_homepage_slider_content', array(
			'type'		=> 'theme_mod',
			'default'	=> 'recent_posts',
			'sanitize_callback' => false,
		) );

		$wp_customize->add_control( 'wpex_homepage_slider_content', array(
			'label'		=> __( 'Homepage Slider Content', 'wpex' ),
			'section'	=> 'wpex_slider',
			'settings'	=> 'wpex_homepage_slider_content',
			'type'		=> 'select',
			'choices'	=> $slider_carousel_choices,
			'priority'	=> '1',
		) );

		// Slider Count
		$wp_customize->add_setting( 'wpex_homepage_slider_count', array(
			'type'		=> 'theme_mod',
			'default'	=> '3',
			'sanitize_callback' => false,
		) );

		$wp_customize->add_control( 'wpex_homepage_slider_count', array(
			'label'		=> __( 'Homepage Slider Count', 'wpex' ),
			'section'	=> 'wpex_slider',
			'settings'	=> 'wpex_homepage_slider_count',
			'type'		=> 'text',
			'priority'	=> '2',
		) );

		// Slider exclude posts
		$wp_customize->add_setting( 'wpex_homepage_slider_exclude_posts', array(
			'type'		=> 'theme_mod',
			'sanitize_callback' => false,
		) );

		$wp_customize->add_control( 'wpex_homepage_slider_exclude_posts', array(
			'label'			=> __( 'Exclude Posts', 'wpex' ),
			'section'		=> 'wpex_slider',
			'settings'		=> 'wpex_homepage_slider_exclude_posts',
			'type'			=> 'checkbox',
			'priority'		=> '3',
			'description'	=> __( 'Check this box to exclude posts included in the carousel from the homepage grid.', 'wpex' ),
		) );

		// Slider SlideShow
		$wp_customize->add_setting( 'wpex_homepage_slider_slideshow', array(
			'type'		=> 'theme_mod',
			'default'	=> '',
			'sanitize_callback' => false,
		) );

		$wp_customize->add_control( 'wpex_homepage_slider_slideshow', array(
			'label'		=> __( 'Automatic Slideshow', 'wpex' ),
			'section'	=> 'wpex_slider',
			'settings'	=> 'wpex_homepage_slider_slideshow',
			'type'		=> 'checkbox',
			'priority'	=> '4',
		) );

		// Slider SlideShow Speed
		$wp_customize->add_setting( 'wpex_homepage_slider_slideshow_speed', array(
			'type'		=> 'theme_mod',
			'default'	=> '7000',
			'sanitize_callback' => false,
		) );

		$wp_customize->add_control( 'wpex_homepage_slider_slideshow_speed', array(
			'label'		=> __( 'Slideshow Speed', 'wpex' ),
			'section'	=> 'wpex_slider',
			'settings'	=> 'wpex_homepage_slider_slideshow_speed',
			'type'		=> 'text',
			'priority'	=> '5',
		) );

		// Slider Alt
		$wp_customize->add_setting( 'wpex_homepage_slider_alt', array(
			'type'              => 'theme_mod',
			'sanitize_callback' => false,
		) );

		$wp_customize->add_control( 'wpex_homepage_slider_alt', array(
			'label'    => __( 'Homepage Slider Alternative', 'wpex' ),
			'section'  => 'wpex_slider',
			'settings' => 'wpex_homepage_slider_alt',
			'type'     => 'textarea',
		) );


		/*-----------------------------------------------------------------------------------*/
		/*	- Homepage Carousel
		/*-----------------------------------------------------------------------------------*/
		
		// Theme Settings Section
		$wp_customize->add_section( 'wpex_carousel' , array(
			'title'		=> __( 'Homepage Carousel', 'wpex' ),
			'priority'	=> 2,
			'panel'		=> 'wpex_homepage',
		) );

		// Carousel Heading
		$wp_customize->add_setting( 'wpex_homepage_carousel_heading', array(
			'type'		=> 'theme_mod',
			'default'	=> __( 'Featured', 'wpex' ),
			'sanitize_callback' => false,
		) );

		$wp_customize->add_control( 'wpex_homepage_carousel_heading', array(
			'label'		=> __( 'Homepage Carousel Heading', 'wpex' ),
			'section'	=> 'wpex_carousel',
			'settings'	=> 'wpex_homepage_carousel_heading',
			'priority'	=> '1',
		) );

		// Carousel Content
		$wp_customize->add_setting( 'wpex_homepage_carousel_content', array(
			'type'		=> 'theme_mod',
			'default'	=> 'recent_posts',
			'sanitize_callback' => false,
		) );

		$wp_customize->add_control( 'wpex_homepage_carousel_content', array(
			'label'		=> __( 'Homepage Carousel Content', 'wpex' ),
			'section'	=> 'wpex_carousel',
			'settings'	=> 'wpex_homepage_carousel_content',
			'type'		=> 'select',
			'choices'	=> $slider_carousel_choices,
			'priority'	=> '2',
		) );

		// Carousel Count
		$wp_customize->add_setting( 'wpex_homepage_carousel_count', array(
			'type'		=> 'theme_mod',
			'default'	=> '3',
			'sanitize_callback' => false,
		) );

		$wp_customize->add_control( 'wpex_homepage_carousel_count', array(
			'label'		=> __( 'Homepage Carousel Count', 'wpex' ),
			'section'	=> 'wpex_carousel',
			'settings'	=> 'wpex_homepage_carousel_count',
			'type'		=> 'text',
			'priority'	=> '3',
		) );

		// Carousel exclude posts
		$wp_customize->add_setting( 'wpex_homepage_carousel_exclude_posts', array(
			'type'		=> 'theme_mod',
			'sanitize_callback' => false,
		) );

		$wp_customize->add_control( 'wpex_homepage_carousel_exclude_posts', array(
			'label'			=> __( 'Exclude Posts', 'wpex' ),
			'section'		=> 'wpex_carousel',
			'settings'		=> 'wpex_homepage_carousel_exclude_posts',
			'type'			=> 'checkbox',
			'priority'		=> '4',
			'description'	=> __( 'Check this box to exclude posts included in the carousel from the homepage grid.', 'wpex' ),
		) );

	}
}
add_action( 'customize_register', 'wpex_customizer_homepage' );