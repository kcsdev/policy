Dear customer, First of all, thank you for your purchase!

Documentation can be located online : http://wpexplorer-themes.com/spartan/

If you happen to face some difficulties with this theme, consider to use our support which is conducted through ThemeForest - http://themeforest.net/user/WPExplorer?ref=WPExplorer

Thank you,
AJ Clarke @ WPExplorer

All the best and have a nice day!