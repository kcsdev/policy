<?php
/**
 * Template Name: Login
 *
 * @package WordPress
 * @subpackage Spartan WPExplorer Theme
 * @since Spartan 1.0
 */

get_header();


// Always check if session is started before write a session variable
if(session_status() == PHP_SESSION_DISABLED){
    session_start();
}

$http_ref = $_SERVER['HTTP_REFERER'];

// It's pointless to save empty referer
if(!empty($http_ref) && $http_ref != site_url("register") && $http_ref != site_url("login")){
    $_SESSION['ref'] = $http_ref;
}

function wp_login_form_kcs( $args = array() ) {
		$defaults = array(
			'echo' => true,
			'redirect' => ( is_ssl() ? 'https://' : 'http://' ) . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], // Default redirect is back to the current page
			'form_id' => 'loginform',
			'label_username' => __( 'Username' ),
			'label_password' => __( 'Password' ),
			'label_remember' => __( 'Remember Me' ),
			'label_log_in' => __( 'Log In' ),
			'id_username' => 'user_login',
			'id_password' => 'user_pass',
			'id_remember' => 'rememberme',
			'id_submit' => 'wp-submit',
			'remember' => true,
			'value_username' => '',
			'value_remember' => false, // Set this to true to default the "Remember me" checkbox to checked
		);

		/**
		 * Filter the default login form output arguments.
		 *
		 * @since 3.0.0
		 *
		 * @see wp_login_form()
		 *
		 * @param array $defaults An array of default login form arguments.
		 */
		$args = wp_parse_args( $args, apply_filters( 'login_form_defaults', $defaults ) );

		/**
		 * Filter content to display at the top of the login form.
		 *
		 * The filter evaluates just following the opening form tag element.
		 *
		 * @since 3.0.0
		 *
		 * @param string $content Content to display. Default empty.
		 * @param array  $args    Array of login form arguments.
		 */
		$login_form_top = apply_filters( 'login_form_top', '', $args );

		/**
		 * Filter content to display in the middle of the login form.
		 *
		 * The filter evaluates just following the location where the 'login-password'
		 * field is displayed.
		 *
		 * @since 3.0.0
		 *
		 * @param string $content Content to display. Default empty.
		 * @param array  $args    Array of login form arguments.
		 */
		$login_form_middle = apply_filters( 'login_form_middle', '', $args );

		/**
		 * Filter content to display at the bottom of the login form.
		 *
		 * The filter evaluates just preceding the closing form tag element.
		 *
		 * @since 3.0.0
		 *
		 * @param string $content Content to display. Default empty.
		 * @param array  $args    Array of login form arguments.
		 */
		$login_form_bottom = apply_filters( 'login_form_bottom', '', $args );
		$temp = wp_lostpassword_url();
		$form = '
		<div class="innerLoginDiv">
		<form name="' . $args['form_id'] . '" id="' . $args['form_id'] . '" action="' . esc_url( site_url( 'wp-login.php', 'login_post' ) ) . '" method="post">
			<h3 class=" loginTitle">
			משתמשים רשומים
			</h3>
			' . $login_form_top . '

			<p class="login-username">
				<label for="' . esc_attr( $args['id_username'] ) . '">' . esc_html( $args['label_username'] ) . '</label>
				<input required="required" type="text" name="log" id="' . esc_attr( $args['id_username'] ) . '" class="input" value="' . esc_attr( $args['value_username'] ) . '" size="20" />
			</p>
			<p class="login-password">
				<label for="' . esc_attr( $args['id_password'] ) . '">' . esc_html( $args['label_password'] ) . '</label>
				<input required="required" type="password" name="pwd" id="' . esc_attr( $args['id_password'] ) . '" class="input" value="" size="20" />
			</p>
			' . $login_form_middle . '
			' . ( $args['remember'] ? '<p class="login-remember"><label><input name="rememberme" type="checkbox" id="' . esc_attr( $args['id_remember'] ) . '" value="forever"' . ( $args['value_remember'] ? ' checked="checked"' : '' ) . ' /> ' . esc_html( $args['label_remember'] ) . '</label></p>' : '' ) . '
			<div class="loginSubmit ">

			<p  class="LoginButton ">
				<input align="center" type="submit"  name="wp-submit" id="' . esc_attr( $args['id_submit'] ) . '" class="cleanslate"  value="' . esc_attr( $args['label_log_in'] ) . '" />
				<input type="hidden" name="redirect_to" value="' . esc_url( $args['redirect'] ) . '" />

			</p>

			<a href="'.$temp.'" class="forgotPass">
			שכחת את הסיסמה?
			</a>
			</div>
			' . $login_form_bottom . '
		</form></div>';

		if ( $args['echo'] )
			echo $form;
		else
			return $form;
	}

?>


	<div id="primary" class="content-area clr">

		<div id="content" class="site-content left-content" role="main">
			<?php
			// Start loop
			while ( have_posts() ) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" class="clr">
					<div class="entry clr">
						<div class="login-template-content clr">
							<?php the_content(); ?>
						</div><!-- .login-template-content -->
						<div class="loginHead"
							<h2><?php _e( 'כניסה / הרשמה', 'wpex' ); ?></h2>

						</div>

						<div class="login-template-forms clr">

							<?php
							// If user is already logged in
							if( is_user_logged_in() ) { ?>
							<style>
								.newUsersDiv{display: none;}
							</style>
								<div class="already-loggedin-msg clr">
									<p><?php
									// Get current user and display already logged in message
									$current_user = wp_get_current_user();
									echo __( 'הינך רשום כבר כ:', 'wpex' ) .' <span>'. $current_user->display_name; ?><span></p>

									<a href="<?php echo wp_logout_url( get_permalink() ); ?>" class="theme-button light"><?php _e( 'התנתק', 'wpex' ); ?></a>
								</div><!-- .already-loggedin-msg -->
							<?php }
							// Display login form & register form
							else { ?>
								<div class="login-form clr">

									<?php
									/**
									 * Display login form
									 *
									 * @link http://codex.wordpress.org/Function_Reference/wp_login_form
									 */
									wp_login_form_kcs( array(
										'label_username'	=> 'שם משתמש',
										'label_password'	=> 'סיסמה',
										'remember'			=> false,
									) ); ?>

								</div><!-- .login-form -->



							<?php } ?>


							<div class="newUsersDiv" id="newUsersDiv">
								<div class="innerLoginDiv">
									<div class="newUserInfo">
										<h3>משתמשים חדשים</h3>
										<h3>רוצים לקרוא את הכתבות המלאות ולהנות מכל המגזינים ?</h3>
										<h3>הרשמו עכשיו! </h3>
<a href="../register-3/" >
									<p class="infoButton">

									<input class="cleanslate " type="button" value="הרשמה לחודש היכרות">



									</p>
</a>

									</div>
								</div>



								</div><!-- .login-form -->

						</div><!-- .login-template-forms -->
					</div><!-- .entry -->
				</article><!-- #post-<?php the_ID(); ?> -->
			<?php
			// End loop
			endwhile; ?>
		</div><!-- #content -->
		<?php
		// Get sidebar if post layout meta isn't set to fullwidth
		if( 'fullwidth' != get_post_meta( get_the_ID(), 'wpex_post_layout', true ) ) {
			get_sidebar();
		} ?>
	</div><!-- #primary -->
<?php get_footer(); ?>