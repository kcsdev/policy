﻿<?php
/**
 * Template Name: registerBad
 *
 * @package WordPress
 * @subpackage Spartan WPExplorer Theme
 * @since Spartan 1.0
 */
get_header();?>
 
<?php 
if ( isset( $_POST['adduser'] ) && isset( $_POST['add-nonce'] ) && wp_verify_nonce( $_POST['add-nonce'], 'add-user' ) ) {

// Die if the nonce fails
if ( ! wp_verify_nonce( $_POST['add-nonce'],'add-user' ) ) {
wp_die( 'Sorry! That was secure, guess you\'re cheatin huh!', 'wpex' );
}

// Setup new user
else {
$userdata = array(
'user_login'		=> esc_attr( $_POST['user_name'] ),
'user_email'		=> esc_attr( $_POST['email'] ),
'user_pass'			=> esc_attr( $_POST['register_user_pass'] ),
'user_pass_repeat'	=> esc_attr( $_POST['register_user_pass_repeat'] ),
'role'				=> get_option( 'default_role' ),
);

// Username check
if ( ! $userdata['user_login'] ) {
$error = __( 'נדרש להזין שם משתמש.', 'wpex' );
} elseif ( username_exists( $userdata['user_login'] ) ) {
$error = __( 'שם משתמש קיים במערכת,נא לבחור שם משתמש אחר.<a href="http://www.c14.co.il/policy/wp-login.php?action=lostpassword" >שכחת סיסמא?</a>', 'wpex' );
}

// Email check
elseif ( ! $userdata['user_email'] ) {
$error = __( 'נדרש להזין כתובת דואר אלקטרוני', 'wpex' );
} elseif ( !is_email( $userdata['user_email'] ) ) {
$error = __( 'כתובת דואר אלקטרוני לא תקינה.', 'wpex' );
} elseif ( email_exists($userdata['user_email'] ) ) {
$error = __( 'כתובת דואר אלקטרוני קיימת במערכת. <a href="http://www.c14.co.il/policy/wp-login.php?action=lostpassword" >שכחת סיסמא?</a>', 'wpex' );
}

// Pass 1 or Password 2
elseif ( ! $userdata['user_pass'] ){
$error = __( 'נדרש להזין סיסמא.', 'wpex' );
}

// Password match
elseif( $userdata['user_pass'] != $userdata['user_pass_repeat'] ){
$error = __( 'סיסמא לא זהה.', 'wpex' );
}

// setup new users and send notification
else{
$new_user = wp_insert_user( $userdata );
wp_new_user_notification( $new_user, $userdata['user_pass'] );
}
}
} ?>



<div class="register-form clr">
									<?php
									// User was created display message
									if ( isset( $new_user ) ) { ?>
                                        <div class="notice green registration-notice">
                                            <?php _e( 'ההרשמה הצליחה, כעת תוכל להתחבר.', 'wpex' ); ?>
                                        </div><!-- .notice -->
                                    <?php }
                                    // User not created, display error
                                    elseif ( !isset( $new_user ) && isset( $error ) && !empty( $error ) ) { ?>
                                        <div class="notice yellow registration-notice">
                                            <?php echo $error; ?>
                                        </div><!-- .notice -->
                                    <?php } ?>
<h2><?php _e( 'הרשם לחשבון חדש', 'wpex' ); ?></h2>
<form method="POST" id="adduser" class="user-forms" action="" autocomplete="off">
    <input class="text-input" name="user_name" type="text" id="user_name" value="<?php echo isset( $_POST['user_name'] ) ? $_POST['user_name'] : ''; ?>" placeholder="<?php echo 'שם משתמש *'; ?>" />
    <input class="text-input" name="phone" type="text" id="user_phone" value="" placeholder="<?php echo 'טלפון *' ?>"/>
    <input class="text-input" name="email" type="text" id="email" value="<?php echo isset( $_POST['email'] ) ? $_POST['email'] : ''; ?>" placeholder="<?php echo 'מייל *'; ?>" />
    <input class="text-input" name="company" type="text" id="user_company" value="" placeholder="<?php echo 'שם החברה *' ?>"/>
    <input class="text-input" name="role" type="text" id="user_role" value="" placeholder="<?php echo 'תפקיד *' ?>"/>
    <input class="text-input" name="register_user_pass" type="password" id="register_user_pass" value="" placeholder="<?php echo 'סיסמה *'; ?>" />
    <input class="text-input" name="register_user_pass_repeat" type="password" id="register_user_pass_repeat" value="" placeholder="<?php echo 'אימות סיסמה *'; ?>" />

    <div class="strength"><span><?php _e( 'Strength indicator', 'wpex' ); ?></span></div>
    <p class="form-submit">
        <input name="adduser" type="submit" id="addusersub" class="submit button" value="הרשם עכשיו" />
        <?php wp_nonce_field( 'add-user', 'add-nonce' ) ?>
        <input name="action" type="hidden" id="action" value="adduser" />
    </p>
</form>
<?php
// Enqueue password strength js
wp_enqueue_script( 'password-strength-meter' ); ?>
<script>
    // <![CDATA[
    jQuery(function(){
        // Password check
        function password_strength() {
            var pass = jQuery('#register_user_pass').val();
            var pass2 = jQuery('#register_user_pass_repeat').val();
            var user = jQuery('#user_name').val();
            jQuery('.strength').removeClass('short bad good strong empty mismatch');
            if ( !pass ) {
                jQuery('#pass-strength-result').html( pwsL10n.empty );
                return;
            }
            var strength = passwordStrength(pass, user, pass2);
            if ( 2 == strength )
                jQuery('.strength').addClass('bad').html( pwsL10n.bad );
            else if ( 3 == strength )
                jQuery('.strength').addClass('good').html( pwsL10n.good );
            else if ( 4 == strength )
                jQuery('.strength').addClass('strong').html( pwsL10n.strong );
            else if ( 5 == strength )
                jQuery('.strength').addClass('mismatch').html( pwsL10n.mismatch );
            else
                jQuery('.strength').addClass('short').html( pwsL10n.short );
        }
        jQuery('#register_user_pass, #register_user_pass_repeat').val('').keyup( password_strength );
    });
    pwsL10n = {
        empty: "<?php _e( 'Strength indicator', 'wpex' ) ?>",
        short: "<?php _e( 'Very weak', 'wpex' ) ?>",
        bad: "<?php _e( 'Weak', 'wpex' ) ?>",
        good: "<?php _e( 'Medium', 'wpex' ) ?>",
        strong: "<?php _e( 'Strong', 'wpex' ) ?>",
        mismatch: "<?php _e( 'Mismatch', 'wpex' ) ?>"
    }
    try{convertEntities(pwsL10n);}catch(e){};
    // ]]>
</script>
</div><!-- .register-form -->


<?php get_footer(); ?>