<?php
/**
 * Template Name: Register
 *
 * @package WordPress
 * @subpackage Spartan WPExplorer Theme
 * @since Spartan 1.0
 */

if ( isset( $_POST['adduser'] ) && isset( $_POST['add-nonce'] ) && wp_verify_nonce( $_POST['add-nonce'], 'add-user' ) ) {

    // Die if the nonce fails
    if ( ! wp_verify_nonce( $_POST['add-nonce'],'add-user' ) ) {
        wp_die( 'Sorry! That was secure, guess you\'re cheatin huh!', 'wpex' );
    } else {
        // Setup new user
        $userdata = array(
        'user_login'		=> esc_attr( $_POST['user_name'] ),
        'user_email'		=> esc_attr( $_POST['email'] ),
        'user_pass'			=> esc_attr( $_POST['register_user_pass'] ),
        'user_pass_repeat'	=> esc_attr( $_POST['register_user_pass_repeat'] ),
        'role'				=> get_option( 'default_role' ),
        );

        // Username check
        if ( ! $userdata['user_login'] ) {
            $error = __( 'חובה להזין <strong>שם משתמש</strong>.', 'wpex' );
        } elseif ( username_exists( $userdata['user_login'] ) ) {
            $error = __( 'שם משתמש קיים במערכת,נא להזין שם משתמש אחר!<a href="http://www.c14.co.il/policy/wp-login.php?action=lostpassword" >שכחת סיסמא?</a>', 'wpex' );
        } elseif ( preg_match('/^[a-z0-9 .\-]+$/i', $userdata['user_login']) == false) {
            $error = __("שם המשתמש שהוזן לא תקין, אנא השתמשו באותיות לועזיות בלבד", "wpex");

        } elseif ( ! $userdata['user_email'] ) {
            // Email check
            $error = __( 'חובה להזין <strong>מייל</strong>.', 'wpex' );
        } elseif ( !is_email( $userdata['user_email'] ) ) {
            $error = __( 'חובה להזין מייל תקין.', 'wpex' );
        } elseif ( email_exists($userdata['user_email'] ) ) {
            $error = __( 'מייל זה קיים במערכת.<a href="http://www.c14.co.il/policy/wp-login.php?action=lostpassword" >שכחת סיסמא?</a>', 'wpex' );
        } elseif ( ! $userdata['user_pass'] ){
            // Pass 1 or Password 2
            $error = __( 'חובה להזין <strong>סיסמא</strong>.', 'wpex' );
        } elseif( ! $userdata['phone'] ){
            $error = __('חובה להזין <strong>טלפון</strong>.', 'wpex');
        } elseif( $userdata['user_pass'] != $userdata['user_pass_repeat'] ){
            // Password match
            $error = __( 'שתי הסיסמאות אינן תואמות.', 'wpex' );
        } else {
            $user_id = wp_insert_user( $userdata );
            wp_new_user_notification( $user_id, $userdata['user_pass'] );

            // Log in user after register
            if($user_id > 0){
                $user = get_user_by( 'id', $user_id );

                if( $user ) {

                    $creds = array();
                    $creds['user_login'] = $userdata['user_login'];
                    $creds['user_password'] = $userdata['user_pass'];
                    $creds['remember'] = true;
                    $user = wp_signon( $creds, false );
                    if ( is_wp_error($user) )
                        echo $user->get_error_message();

                    $url = site_url();

                    if (isset($_SESSION['ref'])) {
                        $url = $_SESSION['ref'];
                    }

                    echo "<script>";
                    echo "window.location = '" . $url . "'";
                    echo "</script>";
                    die();
                } else {
                    echo $sign_result->get_error_message();
                }
            }
        }
    }
} else {

}

get_header();

?>
<div id="primary" class="content-area clr">
        <div id="content" class="site-content left-content clr" role="main">
<div class="register-form clr">
									<?php
									// User was created display message
									if ( isset( $new_user ) ) { ?>
                                        <div class="notice green registration-notice">
                                            <?php _e( 'ההרשמה הצליחה , כעת תוכל <a href="http://www.c14.co.il/policy/login/" >להתחבר</a>.', 'wpex' ); ?>
                                        </div><!-- .notice -->
                                    <?php }
                                    // User not created, display error
                                    elseif ( !isset( $new_user ) && isset( $error ) && !empty( $error ) ) { ?>
                                        <div class="notice yellow registration-notice">
                                            <?php echo $error; ?>
                                        </div><!-- .notice -->
                                    <?php } ?>
<h2 id="reg-head"><?php _e( 'הרשמה לחודש היכרות', 'wpex' ); ?></h2>
<form method="POST" id="adduser" class="user-forms" action="" autocomplete="off">
    <div class="form-heads text-reg"> שם משתמש<span class="star-reg">*</span> </div> 
    <input class="text-input input-reg" name="user_name" type="text" id="user_name" value="<?php echo isset( $_POST['user_name'] ) ? $_POST['user_name'] : ''; ?>" placeholder=" " />
     <div class="form-heads text-reg "> טלפון <span class="star-reg">*</span> </div>
    <input class="text-input input-reg" name="phone" type="text" id="user_phone" value="" placeholder=" "/>
     <div class="form-heads text-reg"> מייל <span class="star-reg">*</span>  </div>
    <input class="text-input input-reg" name="email" type="text" id="email" value="<?php echo isset( $_POST['email'] ) ? $_POST['email'] : ''; ?>" placeholder=" " />
     <div class="form-heads text-reg"> חברה/ארגון </div>
    <input class="text-input input-reg" name="company" type="text" id="user_company" value="" placeholder=" "/>
     <div class="form-heads text-reg"> תפקיד </div>
    <input class="text-input input-reg" name="role" type="text" id="user_role" value="" placeholder=" "/>
     <div class="form-heads text-reg"> סיסמא <span class="star-reg">*</span> </div>
    <input class="text-input input-reg" name="register_user_pass" type="password" id="register_user_pass" value="" placeholder=" " />
     <div class="form-heads text-reg"> אימות סיסמא<span class="star-reg">*</span>  </div>
    <input class="text-input input-reg" name="register_user_pass_repeat" type="password" id="register_user_pass_repeat" value="" placeholder=" " />

  <!--  <div class="strength"><span><?php _e( 'Strength indicator', 'wpex' ); ?></span></div> -->
    <p class="form-submit-1">
        <input name="adduser" type="submit" id="addusersub" class="submit button button-reg" value="הרשם עכשיו" />
        <?php wp_nonce_field( 'add-user', 'add-nonce' ) ?>
        <input name="action" type="hidden" id="action" value="adduser" />
    </p>

</form>

    
<?php
// Enqueue password strength js
wp_enqueue_script( 'password-strength-meter' ); ?>
<script>
    // <![CDATA[
    jQuery(function(){
        // Password check
        function password_strength() {
            var pass = jQuery('#register_user_pass').val();
            var pass2 = jQuery('#register_user_pass_repeat').val();
            var user = jQuery('#user_name').val();
            jQuery('.strength').removeClass('short bad good strong empty mismatch');
            if ( !pass ) {
                jQuery('#pass-strength-result').html( pwsL10n.empty );
                return;
            }
            var strength = passwordStrength(pass, user, pass2);
            if ( 2 == strength )
                jQuery('.strength').addClass('bad').html( pwsL10n.bad );
            else if ( 3 == strength )
                jQuery('.strength').addClass('good').html( pwsL10n.good );
            else if ( 4 == strength )
                jQuery('.strength').addClass('strong').html( pwsL10n.strong );
            else if ( 5 == strength )
                jQuery('.strength').addClass('mismatch').html( pwsL10n.mismatch );
            else
                jQuery('.strength').addClass('short').html( pwsL10n.short );
        }
        jQuery('#register_user_pass, #register_user_pass_repeat').val('').keyup( password_strength );
    });
    pwsL10n = {
        empty: "<?php _e( 'Strength indicator', 'wpex' ) ?>",
        short: "<?php _e( 'Very weak', 'wpex' ) ?>",
        bad: "<?php _e( 'Weak', 'wpex' ) ?>",
        good: "<?php _e( 'Medium', 'wpex' ) ?>",
        strong: "<?php _e( 'Strong', 'wpex' ) ?>",
        mismatch: "<?php _e( 'Mismatch', 'wpex' ) ?>"
    }
    try{convertEntities(pwsL10n);}catch(e){};
    // ]]>
</script>

</div><!-- .register-form -->

</div><!-- content-->
<?php
        // Get sidebar if post layout meta isn't set to fullwidth
        if( 'fullwidth' != get_post_meta( get_the_ID(), 'wpex_post_layout', true ) ) {
            get_sidebar();
        } ?>
</div> <!--content-->

<?php get_footer(); ?>