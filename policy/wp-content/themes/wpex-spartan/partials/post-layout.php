<?php
/**
 * Single post layout
 *
 * @package     Spartan
 * @subpackage  Partials
 * @author      Alexander Clarke
 * @copyright   Copyright (c) 2014, Symple Workz LLC
 * @link        http://www.wpexplorer.com
 * @since       1.1.2
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

// Get theme_mods
$share_position = get_theme_mod( 'share_position', 'post_bottom' ); ?>

<article class="single-post-article clr">
	<?php wpex_ad_spot( 'post-before' ); ?>
	<?php get_template_part( 'partials/post', 'media' ); ?>
	<?php get_template_part( 'partials/post', 'header' ); ?>
	<?php if ( 'post_top' == $share_position ) : ?>
		<?php get_template_part( 'partials/post', 'share' ); ?>
	<?php endif; ?>
	<?php get_template_part( 'partials/post', 'content' ); ?>
	<?php get_template_part( 'partials/post', 'tags' ); ?>
    <?php echo do_shortcode('[whatsapp]'); ?>
	<?php if ( 'post_bottom' == $share_position ) : ?>
		<?php get_template_part( 'partials/post', 'share' ); ?>
	<?php endif; ?>
	<?php get_template_part( 'partials/post', 'pagination' ); ?>
	<?php // get_template_part( 'partials/post', 'author' ); ?>
	
	<?php get_template_part( 'partials/post', 'related' ); ?>
	<?php comments_template(); ?>
	<?php get_template_part( 'partials/post', 'edit' ); ?>
</article>