<?php
/**
 * Single post content
 *
 * @package     Spartan
 * @subpackage  Partials
 * @author      Alexander Clarke
 * @copyright   Copyright (c) 2014, Symple Workz LLC
 * @link        http://www.wpexplorer.com
 * @since       1.1.2
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
} ?>

<div class="entry clr">
	<?php
	// Post top ad
	wpex_ad_spot( 'post-top' );

	// Post Content
		// Function to manipulate the content if user is not allowed to see
		// Case 1 = the_content(); with limited text letters.
		// Case 2 = the_content(); fully appeared.
	$user_ID = get_current_user_id();
	global $wpdb;
	$results = $wpdb->get_results("SELECT *  FROM `wp_m_membership_relationships` WHERE `user_id` = ".$user_ID, OBJECT );
	if($results[0]->level_id == 1 || $results[0]->level_id == 2 || current_user_can( 'manage_options' )){
		return the_content();
	}
	else{

		echo "<h3>רוצים להמשיך לקרוא?</h3>";
		echo "לכתבה המלאה ";
		echo '<a href="http://www.c14.co.il/policy/register-3/">הרשמו לאתר</a> ';
		echo "כבר יש לכם מנוי? ";
		echo '<a href="http://www.c14.co.il/policy/login/">לחצו כאן</a> '; 
		echo "כדי להתחבר לאתר.";

	}
	
	// Paginate posts when using <!--nextpage-->
	wp_link_pages( array(
		'before'		=> '<div class="page-links clr">',
		'after'			=> '</div>',
		'link_before'	=> '<span>',
		'link_after' 	=> '</span>',
	) );

	// Post bottom ad
	wpex_ad_spot( 'post-bottom' );  ?>
</div><!-- .entry -->