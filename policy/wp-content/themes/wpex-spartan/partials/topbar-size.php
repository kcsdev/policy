<?php
/**
 * Topbar date
 *
 * @package     Spartan
 * @subpackage  Partials
 * @author      Alexander Clarke
 * @copyright   Copyright (c) 2014, Symple Workz LLC
 * @link        http://www.wpexplorer.com
 * @since       1.1.2
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

// Return if disabled
if ( ! get_theme_mod( 'wpex_topbar_size', true ) ) {
    return;
} ?>

<div id="topbar-size" class="clr">
    <span><?php echo do_shortcode("[wpa_toolbar]"); ?>
</span>

</div><!-- .topbar-date -->