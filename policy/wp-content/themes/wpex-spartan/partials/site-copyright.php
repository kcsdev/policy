<?php
/**
 * Ouputs the copyright info
 *
 * @package     Spartan
 * @subpackage  Partials
 * @author      Alexander Clarke
 * @copyright   Copyright (c) 2014, Symple Workz LLC
 * @link        http://www.wpexplorer.com
 * @since       1.1.2
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

// Get copyright data
$copyright = get_theme_mod( 'wpex_copyright', 'Copyright 2015 Policy' ); ?>

<div id="copyright" class="clr" role="contentinfo">
	<p style="float: right;"><i class="fa fa-copyright"></i> כל הזכויות שמורות לפוליסה</p>
<p style="float: left;">
    <a href="http://www.kcsnet.net/"><img src="/policy/wp-content/uploads/logo/New-Logo-White.png" width="45">
    <span style="float:right; line-height:1.6; margin-left:5px;"> 
        בניית אתרים</a>
    </span>
</p>
</div><!-- #copyright -->