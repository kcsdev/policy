<?php
/**
 * Single related posts
 *
 * @package     Spartan
 * @subpackage  Partials
 * @author      Alexander Clarke
 * @copyright   Copyright (c) 2014, Symple Workz LLC
 * @link        http://www.wpexplorer.com
 * @since       1.1.2
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

// Return if disabled via meta
if ( ! get_theme_mod( 'wpex_related', true ) ) {
    return;
}

// Return if disabled via theme mods
if ( 'on' == get_post_meta( get_the_ID(), 'wpex_disable_related', true ) ) {
    return;
}

// Get Current post
$post_id = get_the_ID();

// Return if not standard post type
if ( 'post' != get_post_type( $post_id ) ) {
    return;
}

// Create an array of current category ID's
$cats = wp_get_post_terms( $post_id, 'category' ); 
$cats_ids = array();  
foreach( $cats as $wpex_related_cat ) {
    $cats_ids[] = $wpex_related_cat->term_id; 
}

// Related exclude formats
$exclude_formats = array( 'post-format-quote', 'post-format-link', 'post-format-status' );
$exclude_formats = apply_filters( 'wpex_related_posts_exclude_formats', $exclude_formats );

// Related query arguments
$args = array(
    'posts_per_page'        => '8',
    'orderby'               => 'rand',
    'category__in'          => $cats_ids,
    'post__not_in'          => array( $post_id ),
    'no_found_rows'         => true,
    'meta_key'              => '_thumbnail_id',
    'tax_query'             => array (
    'relation'  => 'AND',
        array (
            'taxonomy'  => 'post_format',
            'field'     => 'slug',
            'terms'     => $exclude_formats,
            'operator'  => 'NOT IN',
        ),
    ),
);
$args       = apply_filters( 'wpex_related_posts_args', $args );
$wpex_query = new wp_query( $args );
$counter = 0;
if ( $wpex_query->have_posts() ) { ?>
    <div class="wrap-related">
        <div class="headingA"><?php _e( 'כתבות דומות', 'wpex' ); ?></div>
        <div class="wrap-related-content clr">
          



 <div class="list-related">
    <ul class="b">
<?php

$counter=0;
 foreach( $wpex_query->posts as $post ) : setup_postdata( $post ); ?>
            <?php
            // Display post thumbnail
            if ( has_post_thumbnail() && $wpex_img = wpex_image( 'array', '', true ) ) { ?>
               
                    <?php if($counter <2 )
                    {?>
                       
                        <?php $counter++; ?>


                    
                       
                    <?php } else { ?>
                    <li>
                        <div class="nonePictureRelatedItem">
                        <a class="related-link" href="<?php the_permalink(); ?>" title="<?php echo esc_attr( the_title_attribute( 'echo=0' ) ); ?>">

                            <?php the_title(); ?>
                            <?php $counter++; ?>


                        </a>
                    </div>
                </li>
                    <?php } ?>
                
            <?php } ?>
            <?php endforeach; ?>

</ul>
</div><!-- .related-carousel-slide -->

  <?php $counter=0;
            // Loop through related posts
            foreach( $wpex_query->posts as $post ) : setup_postdata( $post ); ?>
            <?php
            // Display post thumbnail
            if ( has_post_thumbnail() && $wpex_img = wpex_image( 'array', '', true ) ) { ?>
                <div class=" picture-item">
                    <?php if($counter <2 )
                    {?>
                        <div class="pictureRelatedItem">
                             <img class="related-img " src="<?php echo $wpex_img['url']; ?>" alt="<?php echo esc_attr( the_title_attribute( 'echo=0' ) ); ?>" width="<?php echo $wpex_img['width']; ?>" height="<?php echo $wpex_img['height']; ?>" />
                         </br>
                    <a class="related-img-a" href="<?php the_permalink(); ?>" title="<?php echo esc_attr( the_title_attribute( 'echo=0' ) ); ?>">
                       
                       <div class="related-a-content"> <?php the_title(); ?> </div>
                        <?php $counter++; ?>


                    </a>
                        </div>
                    <?php }
                    else{
                      break;  
                    } ?>
                </div><!-- .related-carousel-slide -->
            <?php } ?>
            <?php endforeach; ?>


        </div><!-- .related-carousel -->
    </div>
<?php } // End related items

// Reset post data
wp_reset_postdata();