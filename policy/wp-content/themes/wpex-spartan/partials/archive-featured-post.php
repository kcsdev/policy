<?php
/**
 * Displays the featured entry for categories
 *
 * @package		Spartan
 * @subpackage	Partials
 * @author		Alexander Clarke
 * @copyright	Copyright (c) 2014, Symple Workz LLC
 * @link		http://www.wpexplorer.com
 * @since		1.1.2
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
} ?>

<article class="archive-featured-post clr">
	<div class="archive-featured-post-media clr">

			<div id="home-slider-wrap" class="clr">
<?
				wpex_category_tag( get_query_var( 'cat' ), true ); ?>


				 
 <div class="home-slider-slide" style="position:relative;height: auto; width: 48%; float:right;" data-dot="1">
							<div class="home-slider-media">
								<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( the_title_attribute( 'echo=0' ) ); ?>">
									<?php $img = wpex_image( 'array' ); ?>
								<img class="archive-featured-pic" height="300" src="<?php echo $img['url']; ?>" alt="<?php echo esc_attr( the_title_attribute( 'echo=0' ) ); ?>" width="<?php echo $img['width']; ?>" height="<?php echo $img['height']; ?>" />				</a>
							</div><!-- .home-slider-media -->
							



						</div><div class="home-slider-slide" style="position:relative;height: auto; width: 48%; float:left;" data-dot="1">
							
							<div class="home-slider-caption clr" style="position:static;margin-bottom: 20px; background:none;padding: 0;">
								<h2 class="home-slider-caption-title">
									<a id="home-slider-special-title" href="<?php the_permalink(); ?>" style="color:#474747;font-weight: bold;" title="<?php echo esc_attr( the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
								</h2>
																	<div class="home-slider-caption-excerpt clr" style="color:#474747;">
																		<?php $temp_content=get_the_content(); $temp2=wp_trim_words( $temp_content, $num_words = 90, $more = "..." ); echo $temp2;?>


																		</div><!-- .home-slider-caption-excerpt -->
															</div><!--.home-slider-caption -->
						</div>
		</div>


		
</article><!-- .archive-featured-post -->