<?php
/**
 * Single post meta
 *
 * @package     Spartan
 * @subpackage  Partials
 * @author      Alexander Clarke
 * @copyright   Copyright (c) 2014, Symple Workz LLC
 * @link        http://www.wpexplorer.com
 * @since       1.1.2
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

// Get array and set defaults
$array = get_theme_mod( 'wpex_post_meta', array( 'time', 'date', 'author', 'category', 'comments' ) );

// Return if empty
if ( empty( $array ) ) {
	return;
}

// Turn into array
if ( ! is_array( $array ) ) {
	$array = explode( ',', $array );
} ?>

<div class="post-meta clr">
	<?php
	// Swap order if RTL
	if ( is_rtl() ) {
		$array = array_reverse( $array );
	}
	// Loop through meta options
	foreach( $array as $item ) {
	switch( $item ) {
		// Date
		case 'date': ?>
		<span class="post-meta-date">
			<?php _e( 'בתאריך', 'wpex' ); ?> <?php echo get_the_date(); ?>
		</span>
		<?php break; 
        // Time
		case 'time': ?>
		<span class="post-meta-time">
			<?php _e( 'בשעה', 'wpex' ); ?> <?php echo the_time('G:i') ?>
		</span>
		<?php break;    
		// Category
		case 'category': ?>
		<span class="post-meta-category">
			<?php _e( 'מדור', 'wpex' ); ?> <?php the_category(', '); ?>
		</span>
		<?php break;
		// Comments
		case 'comments': ?>
		<?php if ( comments_open() ) { ?>
<!--
			<span class="post-meta-comments">
				<?php _e( 'עם', 'wpex' ); ?> <?php comments_popup_link( __( '0 Comments', 'wpex' ), __( '1 Comment',  'wpex' ), __( '% תגובות', 'wpex' ), 'comments-link' ); ?>
-->
			</span>
		<?php } ?>
	<?php } // END switch
	} // End foreach ?>
</div><!-- .post-meta -->