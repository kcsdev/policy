<?php
/**
 * Displays the single post video
 *
 * @package		Spartan
 * @subpackage	Partials
 * @author		Alexander Clarke
 * @copyright	Copyright (c) 2014, Symple Workz LLC
 * @link		http://www.wpexplorer.com
 * @since		1.1.2
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Get post video
$post_video = wpex_post_video();

// Display post video
if ( $post_video ) :  ?>
	<?php echo $post_video; ?>
<?php
// If no video defined, then display the thumbnail
elseif ( has_post_thumbnail() ) :

	// Get featured image, see @inc/featured-image.php
	$img = wpex_image( 'array' );

	// Return if array is empty
	if ( empty( $img ) ) {
		return;
	} ?>
	<div class="post-thumbnail">
		<img src="<?php echo $img['url']; ?>" alt="<?php echo esc_attr( the_title_attribute( 'echo=0' ) ); ?>" width="<?php echo $img['width']; ?>" height="<?php echo $img['height']; ?>" />
	</div><!-- .post-thumbnail -->
<?php endif; ?>