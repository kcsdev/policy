<?php
/**
 * Displays the single post thumbnail
 *
 * @package		Spartan
 * @subpackage	Partials
 * @author		Alexander Clarke
 * @copyright	Copyright (c) 2014, Symple Workz LLC
 * @link		http://www.wpexplorer.com
 * @since		1.1.2
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Return if disabled via custom field
if ( 'on' == get_post_meta( get_the_ID(), 'wpex_disable_featured_image', true ) ) {
	return;
}

// Return if there isn't any thumbnail defined
if ( ! has_post_thumbnail() ) {
	return;
} ?>

<div class="page-thumbnail">
	<?php the_post_thumbnail( 'full' ); ?>
</div><!-- .page-thumbnail -->