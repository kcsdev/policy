<?php
/**
 * Footer bottom
 *
 * @package     Spartan
 * @subpackage  Partials
 * @author      Alexander Clarke
 * @copyright   Copyright (c) 2014, Symple Workz LLC
 * @link        http://www.wpexplorer.com
 * @since       1.1.2
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
} ?>

<div id="footer-bottom" class="clr">
    <div class="container clr">
        <?php

            get_template_part( 'partials/site', 'copyright' );

        ?>
    </div><!-- .container -->
</div><!-- #footer-bottom -->