<?php
/**
 * Header branding => logo & site description
 *
 * @package     Spartan
 * @subpackage  Partials
 * @author      Alexander Clarke
 * @copyright   Copyright (c) 2014, Symple Workz LLC
 * @link        http://www.wpexplorer.com
 * @since       1.1.2
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

// Get data
$logo_src       = get_theme_mod( 'wpex_logo' );
$logo_src       = apply_filters( 'wpex_logo_src', $logo_src );
$blog_name      = get_bloginfo( 'name' );
$description    = get_theme_mod( 'wpex_logo_subheading', __( 'Edit your subheading via the theme customizer.', 'wpex' ) );

// Sanitize data
$logo_src       = esc_url( $logo_src );
$esc_blog_name  = esc_attr( $blog_name );
$description    = wpex_sanitize_data( $description, 'html' ); ?>

<div class="site-branding clr">
    <div id="logo" class="clr">
        <?php
        // Get correct tag for SEO
        if ( is_front_page() ) {
            $open_tag = '<h1>';
            $close_tag = '</h1>';
        } else {
            $open_tag = '<h2>';
            $close_tag = '</h2>';
        } ?>
        <?php if ( $logo_src ) {
            echo $open_tag; ?>
            <a href="<?php echo home_url(); ?>" title="<?php echo $esc_blog_name; ?>" rel="home">
                <img src="<?php echo $logo_src; ?>" alt="<?php echo $esc_blog_name; ?>" />
            </a>
        <?php
            echo $close_tag;
        } else { ?>
            <div class="site-text-logo clr">
                <?php echo $open_tag; ?>
                    <a href="<?php echo home_url(); ?>" title="<?php echo $esc_blog_name; ?>" rel="home"><?php echo $blog_name; ?></a>
                <?php echo $close_tag; ?>
            </div>
        <?php } ?>
    </div><!-- #logo -->




    <?php if ( $description ) : ?>
        <div id="blog-description" class="clr">
            <?php echo $description; ?>
        </div><!-- #blog-description -->
    <?php endif; ?>
</div><!-- .site-branding -->

<div id="site-navigation-wrap" class="clr">
    <div id="site-navigation-inner" class="clr container">
        <nav id="site-navigation" class="navigation main-navigation clr" role="navigation">
            <?php
            // Check if main menu if defined
            if ( has_nav_menu( 'main_menu' ) ) {
                // Display main nav
                wp_nav_menu( array(
                    'theme_location'    => 'main_menu',
                    'sort_column'       => 'menu_order',
                    'menu_class'        => 'main-nav dropdown-menu sf-menu',
                    'fallback_cb'       => false,
                ) );
                // Display nav toggle for mobile devices
                if ( get_theme_mod( 'wpex_responsive', true ) ) { ?>
                    <a href="#mobile-nav" class="navigation-toggle"><span class="fa fa-bars navigation-toggle-icon"></span><span class="navigation-toggle-text"><?php echo get_theme_mod( 'wpex_mobile_menu_open_text', __( 'Click here to navigate', 'wpex' ) ); ?></span></a>
                <?php } ?>
            <?php }
            // If main menu isn't defined display notice
            elseif ( is_user_logged_in() && current_user_can( 'edit_theme_options' ) ) { ?>
                <div id="site-navigation-notice">
                    <?php echo '<a href="'. admin_url( 'nav-menus.php' ) .'" title="'. __( 'WordPress Menu Editor', 'wpex' ) .'">'. __( 'Create and assign your menu', 'wpex' ) .' &rarr;</a>'; ?>
                </div>
            <?php } ?>
        </nav><!-- #site-navigation -->
    </div><!-- #site-navigation-inner -->
</div><!-- #site-navigation-wrap -->


<div class="topFooterItems">
  
</div><!-- .footer-box -->