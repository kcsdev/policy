<?php
/**
 * The Header for our theme.
 *
 * @package		Spartan
 * @author		Alexander Clarke
 * @copyright	Copyright (c) 2014, Symple Workz LLC
 * @link		http://www.wpexplorer.com
 * @since		1.0.0
 */
?><!DOCTYPE html>
<html>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <?php get_template_part( 'partials/meta', 'viewport' ); ?>
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/my_style.css">
    <link rel="stylesheet" type="text/css" href="../wp-content/themes/wpex-spartan/css/cleanslate.css">
    <?php wp_head(); ?>

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-70611933-1', 'auto');
        ga('send', 'pageview');

    </script>
</head>

<body <?php body_class(); ?>>

<div id="wrap" class="clr">

    <?php get_template_part( 'partials/header', 'layout' ); ?>

    <div class="site-main-wrap clr">
        <div id="main" class="site-main clr container">