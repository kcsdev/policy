<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'c14_wp30');

/** MySQL database username */
define('DB_USER', 'c14_wp30');

/** MySQL database password */
define('DB_PASSWORD', 'U(Se0hK@G@28@,7');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'LrP8p9W7kw2l9dSGsmlDJZuJ5H3zFaMiURprIxV6TiERd8Lb2f32UMs83LbSc1jc');
define('SECURE_AUTH_KEY',  'hpZAYRojE0fl0yLViOoGEAWJILj6FZc5xAN7c3amNnpniIk9CYY0yLQSu2csl5KM');
define('LOGGED_IN_KEY',    'yfeBm2E9HpqONNyJS0K0JuEgZrYXeZI9bHgd6pNLe6Zm8FrxXqSl3HsdqPvRV1V8');
define('NONCE_KEY',        '8HfQxQGr61hjBBDNl6n35YKPpI7jqESCS7z4qFhwNGB7Jj172W3Rx5gA7aO1oTRN');
define('AUTH_SALT',        'by3QpqsmebJyGvlqig5RyR1jkXdl3q8DWkjp7R7XljtQKFRtjuGQ3u5KSov1d6rr');
define('SECURE_AUTH_SALT', '0zLGG4AgMj8vA3MTjQORkoVRHF7y03YPLOhbQkK8eRwkxhbh1XkS4B9tNP8jNMmn');
define('LOGGED_IN_SALT',   'TbVJ0FJD5GMYo7ZQ8C71bSWOOwJlouVuz9O36vWTEnCRLmtF6lGdGZFCUu6UFZeh');
define('NONCE_SALT',       'CDOM2BFRutTWYoJRw2eNr91FgGKQZfXC6Z7uj8S0uJ5duTR59o6akF3gy6L4lEJz');

/**
 * Other customizations.
 */
define('FS_METHOD','direct');define('FS_CHMOD_DIR',0755);define('FS_CHMOD_FILE',0644);
define('WP_TEMP_DIR',dirname(__FILE__).'/wp-content/uploads');

/**
 * Turn off automatic updates since these are managed upstream.
 */
define('AUTOMATIC_UPDATER_DISABLED', true);


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
